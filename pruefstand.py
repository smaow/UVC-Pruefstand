import serial
from datetime import datetime
import time
import pandas as pd 
from collections import deque
import matplotlib.pyplot as plt 
import os


ser =serial.Serial('COM3',9600) #...................................Seriale Komunikation mit Arduino

df=pd.DataFrame(columns=['position','Anzahl_Zyklen','Kraft'])#......Ein leere Dataframe

timestamp=[] #......................................................Liste für Zeitschritte 
position_list=[] #..................................................Liste für Positionen 
kraft_list=[] #.....................................................Liste für kraft-Werte 
zyklen_list=[]#.....................................................Liste für Anzah der Zyklen

data_name=datetime.now()#...........................................Anfangszeit für Dateiname speichern
data_name=data_name.strftime('%Y-%m-%d-%H%M%S')#....................Zeit als Str in Jahr,Monat,Tag,Stunde,Minute und Sekunde formatieren

def update_plot(): #................................................Funkion für Ploten 
    plt.clf()
    plt.plot(timestamp,position_list,label='Positionen')
    plt.plot(timestamp,zyklen_list,label='Zyklen')
    plt.plot(timestamp,kraft_list,label='Kraft')
    plt.xlabel('Zeit')
    plt.ylabel('Messwerte')
    plt.legend()
    plt.pause(0.1)


plt.ion()

while True:
    
    data=ser.readline().decode('utf-8').strip() #................. DAten Zeilenweise von Arduino auslesen 
    if data:
        try:
            data_list=data.split(',') #........................... Erhaltene Daten sind durch Koma in Arduino-code getrennt. Hier in eine Liste umwandeln
            data_list=[int(i) for i in data_list]#................ Daten von Datentype String in Zahlen (int) umwandeln
            zeit=datetime.now() #................................. aktuelle zeit 
            zeitformat=zeit.strftime('%Y-%m-%d %H:%M:%S')#........ Aktuelle Zeit formatieren
            positionen=data_list[0] #............................. Die erste Spalte von daten_list (pos) Variable "positionen" speichrn
            zyklen=data_list[1]#.................................. Die zweite Spalte von daten_list (zyklen) im Variable "zyklen" speichrn speichern
            kraft=data_list[2]#................................... Die dritte Spalte von daten_list (kraft) im Variable "kraft" speichrn speichern
            df.loc[zeitformat]=[positionen,zyklen,kraft]#......... Die einzelne Werte als eine Zeile in Dataframe speichern ( die aktuelle Zeit als index von Dataframe verwenden)

            timestamp.append(zeitformat)#..........................Die einzelne Werte an Liste anhängen 
            position_list.append(positionen)#......................Die einzelne Werte an Liste anhängen 
            zyklen_list.append(zyklen)#............................Die einzelne Werte an Liste anhängen 
            kraft_list.append(kraft)#..............................Die einzelne Werte an Liste anhängen 
            
            if not os.path.isfile(data_name+'csv'):#...............Falls die Datei (file) nicht vorhanden ist 
                df.to_csv(data_name+'.csv',sep=';')#...............erstelle ein File (mit Datum und Uhrzeit als Name)
        
            update_plot() #........................................Funktion von ploten aufrufen 

        except KeyboardInterrupt:
            break
ser.close() #......................................................seriale Komunikation ausschalten 

plt.ioff()#.........................................................Ploten ausschalten
plt.show()
'''
Achtung
'''